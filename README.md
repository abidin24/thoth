# Thoth

The kernel of the Thoth operating system

---

## What is Thoth?

Thoth is a general-purpose, hybrid, UNIX-inspired (but not UNIX-like) operating system designed as an experiment into operating system design and development. Thoth aims to be a hybrid system and is designed such that kernel-level components such as drivers and generic servers may be loaded and unloaded into the system at run-time. As of October 2016, this design goal is unimplemented.

## Design Goals

* UNIX-inspired environment
	* Abstract I/O implemented through filesystem
* Modular kernel structure
* Drivers for most common hardware implemented

## Who develops Thoth?

Thoth is currently primarily developed by me, Joshua Barretto (<joshua.s.barretto@gmail.com>)

---

## What is the Thoth kernel?

The Thoth kernel is an operating system kernel designed to provide an environment for the Thoth operating system. Initially designed as a monolithic entity, later development should see kernel components designed in a modular fashion that allows them to be loaded and unload at run-time.

## How is the Thoth kernel implemented?

The Thoth kernel is written largely in C++, although C and assembly code are used for low-level components and procedures.

## How can the Thoth kernel be booted?

The Thoth kernel requires a Multiboot-compliant bootloader to be booted. All testing is done with GRUB2.