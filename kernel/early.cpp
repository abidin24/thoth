/*
* 	filename  : early.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/early.hpp>
#include <thoth/ibis.hpp>

// Ibis headers
#include <ibis/stdio.hpp>

#if defined(THOTH_ARCH_i686) || defined(THOTH_ARCH_x86_64)
	#include <thoth/x86-family/earlyvga.hpp>
	#include <thoth/x86-family/vga.hpp>
#endif

namespace thoth
{
	void early_clear()
	{
		#if defined(THOTH_ARCH_i686) || defined(THOTH_ARCH_x86_64)
			early_clear_vga();
		#endif
	}

	void early_print(const char* _str)
	{
		if (ibis_initiated()) // If stdout is already initiated, redirect to stdout
		{
			ibis::print(_str);
		}
		else // Otherwise, direct towards early VGA
		{
			#if defined(THOTH_ARCH_i686) || defined(THOTH_ARCH_x86_64)
				if (vga_initiated) // If the main VGA driver had been initiated
					vga_print_string(_str);
				else // Otherwise, default to early VGA
					early_print_vga(_str);
			#endif
		}
	}
};
