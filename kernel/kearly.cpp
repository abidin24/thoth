/*
* 	filename  : kearly.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/string.hpp>
#include <ibis/memory.hpp>
#include <ibis/ref.hpp>
#include <ibis/random.hpp>
#include <ibis/generic/vector.hpp>

// Thoth headers
#include <thoth/kentry.hpp>
#include <thoth/early.hpp>
#include <thoth/kerror.hpp>
#include <thoth/mempool.hpp>

#include <thoth/x86-family/interrupt.hpp>
#include <thoth/x86-family/kbd.hpp>

namespace thoth
{
	extern "C" void kearly()
	{
		kerror_push(__func__);

		early_clear();
		early_print("[ OK ] Entered kernel early\n");
		early_print("[ OK ] Initiated early kernel output\n");

		//interrupt_init();
		//kbd_init();
		//while (true) { asm volatile("hlt"); }

		mempool_init((byte*)0x1000000, 0x100000, 64); // At 16 MB, 1 MB in size, composed of blocks of 64 B

		//ibis::ref<tty> a(new tty());

		//early_print((ibis::string("Kernel Memory Pool Usage = ") + ibis::to_string(mempool_get_usage_glob().data() / 100) + "%\n").data());
		//early_print((ibis::string("Kernel Memory Pool Fragmentation = ") + ibis::to_string(mempool_get_fragmentation_glob().data() / 100) + "%\n").data());

		early_print("[ OK ] Initiated kernel dynamic memory pool\n");

		//kerror("Something isn't working.");

		kerror_pop();
	}
}
