/*
* 	filename  : kmain.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/string.hpp>
#include <ibis/memory.hpp>

// Thoth headers
#include <thoth/kentry.hpp>
#include <thoth/early.hpp>
#include <thoth/kerror.hpp>
#include <thoth/vfs/vfs.hpp>
#include <thoth/console.hpp>
#include <thoth/ibis.hpp>
#include <thoth/random.hpp>
#include <thoth/kprompt.hpp>

#if defined(THOTH_ARCH_i686) || defined(THOTH_ARCH_x86_64)
	#include <thoth/x86-family/vga.hpp>
	#include <thoth/x86-family/serial.hpp>
	#include <thoth/x86-family/interrupt.hpp>
	#include <thoth/x86-family/kbd.hpp>
#endif

namespace thoth
{
	extern "C" void kmain()
	{
		kerror_push(__func__);

		early_print("[ OK ] Entered kernel main\n");

		// Initiate and populate the VFS

		vfs_init();
		early_print("[ OK ] Initiated virtual filesystem\n");

		vfs_populate();
		early_print("[ OK ] Populated virtual filesystem\n");

		ibis::string console_stdin = "/dev/zero";
		ibis::string console_stdout = "/dev/zero";

		#if defined(THOTH_ARCH_i686) || defined(THOTH_ARCH_x86_64)
			// Initiate the VGA driver
			vga_init(vga_inherit_mode::CURSOR);
			early_print("[ OK ] Initiated vga driver\n");

			// Initiate the serial driver
			serial_init("/dev/serial");
			early_print("[ OK ] Initiated serial driver\n");
			serial_init_port(1, 57600, 8, 1, (0x1 << 3));
			early_print("[ OK ] Opened COM1 port\n");

			console_stdout = "/dev/vga";
			//console_stdout = "/dev/serial/com1";

			interrupt_init();
			early_print("[ OK ] Initiated interrupts\n");

			kbd_init();
			early_print("[ OK ] Initiated keyboard\n");
		#endif

		// Initiate console
		console_init(console_stdin, console_stdout);
		early_print("[ OK ] Initiated console driver\n");
		unsigned long kernel_tty = console_gen_tty().data();
		console_switch_tty(kernel_tty);
		early_print("[ OK ] Created kernel console tty\n");

		// Initiate Ibis (create stdout and stdin, etc.)
		ibis_init(console_get_tty(kernel_tty).data(), console_get_tty(kernel_tty).data());
		early_print("[ OK ] Initiated Ibis standard in/out\n");

		// Initiate the random device
		random_init("/dev/random");
		early_print("[ OK ] Initiated random driver\n");

		kprompt();

		while (true) { asm volatile("hlt"); }

		kerror_pop();
	}
}
