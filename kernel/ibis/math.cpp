/*
* 	filename  : math.cpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/math.hpp>

namespace ibis
{
	smemint abs(smemint _i)
	{
		return _i >= 0 ? _i : -_i;
	}

	smemint max(smemint _a, smemint _b)
	{
		return _a > _b ? _a : _b;
	}

	smemint min(smemint _a, smemint _b)
	{
		return _b > _a ? _a : _b;
	}
}
