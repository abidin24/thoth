/*
* 	filename  : random.cpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/random.hpp>

namespace ibis
{
	static unsigned long next_rand = 1;

	int random()
	{
		next_rand = next_rand * 1103515245 + 12345;
		return (unsigned long)(next_rand / 65536) % 32768;
	}

	void random_seed(unsigned int seed)
	{
		next_rand = seed;
	}
}
