/*
* 	filename  : char.cpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/types.hpp>

namespace ibis
{
	char lowercase(char _c)
	{
		return (_c >= 'A' && _c <= 'Z') ? (_c - 'A' + 'a') : _c;
	}

	char uppercase(char _c)
	{
		return (_c >= 'a' && _c <= 'z') ? (_c - 'a' + 'A') : _c;
	}

	bool is_hex(char c)
	{
		if (c >= '0' && c <= '9') return true;
		if (c >= 'a' && c <= 'z') return true;
		if (c >= 'A' && c <= 'Z') return true;
		return false;
	}

	uint8 to_hex(char c)
	{
		if (c >= '0' && c <= '9') return c - '0';
		if (c >= 'a' && c <= 'f') return 10 + c - 'a';
		if (c >= 'A' && c <= 'F') return 10 + c - 'A';
		return 0;
	}
}
