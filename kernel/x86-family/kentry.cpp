/*
* 	filename  : kentry.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/types.hpp>

// Thoth headers
#include <thoth/kentry.hpp>

inline void _kpanic();
inline void _khalt();

namespace thoth
{
	void kpanic(uint32 _err, const char* _msg)
	{
		asm volatile ("mov %0, %%eax \n mov %1, %%ebx \n jmp _kpanic" :: "r"(_err), "r"(_msg));
	}

	void khalt()
	{
		asm volatile ("jmp _khalt");
	}
}
