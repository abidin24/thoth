/*
* 	filename  : serial.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/x86-family/serial.hpp>
#include <thoth/x86-family/port.hpp>
#include <thoth/vfs/vfs.hpp>
#include <thoth/vfs/vnode.hpp>
#include <thoth/early.hpp>

namespace thoth
{
	const int UART_CLOCK_TICK_RATE = 115200;

	enum class serial_port
	{
		COM1 = 0x3F8,
		COM2 = 0x2F8,
		COM3 = 0x3E8,
		COM4 = 0x2E8,
	};

	enum class serial_parity
	{
		NONE =  (0x0 << 3),
		ODD =   (0x1 << 3),
		EVEN =  (0x3 << 3),
		MARK =  (0x5 << 3),
		SPACE = (0x7 << 3),
	};

	static void serial_write_char(unsigned short _port, char _c);
	static bool serial_can_write(unsigned short _port);
	static void serial_write_char(unsigned short _port, char _c);

	ibis::status serial_init(ibis::string _node)
	{
		thoth::vnode* dev_node = nullptr;
		thoth::vnode* serial_node = nullptr;

		// Bind serial driver to /dev/serial file hook
		dev_node = vfs_get_path("/dev");
		if (dev_node != nullptr)
			serial_node = dev_node->add_vnode("serial", VNODE_FLAG_TYPE_DIR);

		if (dev_node == nullptr)
			early_print("serial driver : Failed to find vnode '/dev'\n");
		else if (serial_node == nullptr)
			early_print("serial driver : Failed to create vnode '/dev/serial'\n");
		else
		{
			bool bind_success = serial_node->bind_write_hook(serial_process_msg);

			if (bind_success)
				early_print("serial driver : Successfully bound to '/dev/serial' write hook\n");
			else
				early_print("serial driver : Failed to bind to '/dev/serial' write hook\n");
		}

		return ibis::status(ibis::result::SUCCESS);
	}

	void serial_init_port(unsigned short _port, int _baudrate, byte _databits, byte _stopbits, byte _parity)
	{
		// Bind serial driver to /dev/serial/comX file hook
		thoth::vnode* port_node = nullptr;
		thoth::vnode* serial_node = vfs_get_path("/dev/serial");
		if (serial_node != nullptr)
			port_node = serial_node->add_vnode(ibis::string("com") + ibis::to_string(_port), VNODE_FLAG_TYPE_FILE);

		if (serial_node == nullptr)
			early_print("serial driver : Failed to find vnode '/dev/serial'\n");
		else if (port_node == nullptr)
			early_print((ibis::string("serial driver : Failed to create vnode '/dev/serial/com") + ibis::to_string(_port) + "'\n").data());
		else
		{
			bool bind_success = port_node->bind_write_hook(serial_port_process_msg);

			if (bind_success)
				early_print((ibis::string("serial driver : Successfully bound to '/dev/com") + ibis::to_string(_port) + "' write hook\n").data());
			else
			{
				early_print((ibis::string("serial driver : Failed to bind to '/dev/com") + ibis::to_string(_port) + "' write hook\n").data());
				return;
			}
		}

		unsigned short portval = 0x0;
		unsigned char  databitsval = 0;
		unsigned char  stopbitsval = 0;

		switch (_port)
		{
		case 1:
			portval = 0x3F8;
			break;
		case 2:
			portval = 0x2F8;
			break;
		case 3:
			portval = 0x3E8;
			break;
		case 4:
			portval = 0x2E8;
			break;
		default:
			break;
		}

		// Find databits value equivalent
		switch (_databits)
		{
			case (5):
				databitsval = 0x0;
				break;
			case (6):
				databitsval = 0x1;
				break;
			case (7):
				databitsval = 0x2;
				break;
			case (8):
			default:
				databitsval = 0x3;
				break;
		}

		// Find stopbits value equivalent
		switch (_stopbits)
		{
			case (1):
				stopbitsval = 0x4;
				break;
			case (2):
			default:
				stopbitsval = 0x0;
				break;
		}

		// Calculate a divisor value for the baudrate
		unsigned short divisor = (unsigned short)(UART_CLOCK_TICK_RATE / _baudrate);

		port_out8(portval + 1, 0x00); // Disable all interrupts
		port_out8(portval + 3, 0x80); // Enable DLAB (set baud rate divisor)

		// Divisor short value
		port_out8(portval + 0, ((divisor >> 0) & 0xFF)); // Set divisor (lo byte)
		port_out8(portval + 1, ((divisor >> 8) & 0xFF)); // Set divisor (hi byte)

		unsigned char serial_config = databitsval | stopbitsval | (unsigned char)_parity;
		port_out8(portval + 3, serial_config); // 8 bits, no parity, one stop bit
		port_out8(portval + 2, 0xC7); // Enable FIFO, clear them, with 14-byte threshold
		port_out8(portval + 4, 0x0B); // IRQs enabled, RTS/DSR set
	}

	static bool serial_can_write(unsigned short _port)
	{
		return (port_in8((unsigned short)_port + 5).data() & 0x20) == 0;
	}

	static void serial_write_char(unsigned short _port, char _c)
	{
		unsigned short portval = 0x0;

		switch (_port)
		{
		case 1:
			portval = 0x3F8;
			break;
		case 2:
			portval = 0x2F8;
			break;
		case 3:
			portval = 0x3E8;
			break;
		case 4:
			portval = 0x2E8;
			break;
		default:
			break;
		}

		while (!serial_can_write(_port)) {}

		port_out8(portval, _c);
	}

	void serial_process_msg(const ibis::file_msg _msg, const vfilesession* _session)
	{
		if (_msg.op() == (unsigned int)ibis::file_msg_op::DEFAULT)
			return;
	}

	void serial_port_process_msg(const ibis::file_msg _msg, const vfilesession* _session)
	{
		if (_msg.op() != (unsigned int)ibis::file_msg_op::DEFAULT)
			return;

		for (memint i = 0; i < _msg.size(); i ++)
			serial_write_char(1, (char)(_msg.data()[i]));
	}
};
