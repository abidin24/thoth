/*
* 	filename  : port.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Local headers
#include <thoth/x86-family/port.hpp>

namespace thoth
{
	ibis::status port_out8(uint16 _port, uint8 _value)
	{
		asm volatile ("outb %0, %1" : : "a"(_value), "Nd"(_port));
		return ibis::status(ibis::result::SUCCESS);
	}

	ibis::status port_out16(uint16 _port, uint16 _value)
	{
		asm volatile ("outw %0, %1" : : "a"(_value), "Nd"(_port));
		return ibis::status(ibis::result::SUCCESS);
	}

	ibis::status port_out32(uint16 _port, uint32 _value)
	{
		asm volatile ("outl %0, %1" : : "a"(_value), "Nd"(_port));
		return ibis::status(ibis::result::SUCCESS);
	}

	ibis::value<uint8> port_in8(uint16 _port)
	{
		uint8_t ret;
		asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(_port));
		return ibis::value<uint8>(ret, ibis::result::SUCCESS);
	}

	ibis::value<uint16> port_in16(uint16 _port)
	{
		uint16_t ret;
		asm volatile ("inw %1, %0" : "=a"(ret) : "Nd"(_port));
		return ibis::value<uint16>(ret, ibis::result::SUCCESS);
	}

	ibis::value<uint32> port_in32(uint16 _port)
	{
		uint32_t ret;
		asm volatile ("inl %1, %0" : "=a"(ret) : "Nd"(_port));
		return ibis::value<uint32>(ret, ibis::result::SUCCESS);
	}
}
