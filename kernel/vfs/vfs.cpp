/*
* 	filename  : vfs.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/vfs/vfs.hpp>
#include <thoth/kerror.hpp>
#include <thoth/vfs/vnode.hpp>
#include <thoth/vfs/vfilesession.hpp>

// Ibis headers
#include <ibis/stdio.hpp>

namespace thoth
{
	vnode* vfs_root = nullptr;

	ibis::vector<vfilesession>* vfs_filesessions = nullptr;
	unsigned long vfs_filesession_counter = 1;

	ibis::status vfs_init()
	{
		kerror_push(__func__);

		if (vfs_root != nullptr || vfs_filesessions != nullptr)
			return ibis::status(ibis::result::FAILURE);

		vfs_root = new vnode("root", VNODE_FLAG_TYPE_ROOTDIR);
		vfs_filesessions = new ibis::vector<vfilesession>();

		kerror_pop();
		return ibis::status(ibis::result::SUCCESS);
	}

	ibis::status vfs_populate()
	{
		vnode* dev = vfs_get_root()->add_vnode("dev", VNODE_FLAG_TYPE_DIR);
		vnode* zero = dev->add_vnode("zero", VNODE_FLAG_TYPE_FILE);
		vnode* usr = vfs_get_root()->add_vnode("usr", VNODE_FLAG_TYPE_DIR);
		vnode* joshua = usr->add_vnode("joshua", VNODE_FLAG_TYPE_DIR);
		return ibis::status(ibis::result::SUCCESS);
	}

	vnode* vfs_get_root()
	{
		return vfs_root;
	}

	vnode* vfs_get_path(ibis::string _path)
	{
		if (_path[0] != '/')
			return nullptr;

		return vfs_get_root()->get_path(_path.substr(1));
	}

	static void vfs_display_node(vnode* _vnode, memint _depth = 0)
	{
		kerror_push(__func__);

		for (memint i = 0; i < _depth; i ++)
		{
			if (i != _depth - 1)
				ibis::print("   ");
			else
				ibis::print("$F8 |-$FF");
		}

		if (_vnode->flags().matches(VNODE_FLAG_DIR, VNODE_FLAG_MASK_DIR)) // Directory
		{
			if (_vnode->flags().matches(VNODE_FLAG_ROOT, VNODE_FLAG_MASK_ROOT)) // Root node
				ibis::print(ibis::string(" $F3") + "/" + "$FF");
			else
				ibis::print(ibis::string(" $F3") + _vnode->name() + "$FF/");
		}
		else // Normal file
			ibis::print(ibis::string(" $F2") + _vnode->name() + "$FF");

		if (_vnode->func_write != nullptr)
			ibis::print_format(" $FCwrite_hook$FF : $FC0x%X$FF", (smemint)_vnode->func_write);

		ibis::print("\n");

		for (memint i = 0; i < _vnode->children().length(); i ++)
			vfs_display_node(_vnode->children()[i], _depth + 1);

		kerror_pop();
	}

	void vfs_display()
	{
		kerror_push(__func__);

		ibis::print_line("$F3-- $F4Virtual Filesystem$F3 --$FF");
		vfs_display_node(vfs_get_root());

		kerror_pop();
	}

	static unsigned long vfs_get_unique_filesession_id()
	{
		vfs_filesession_counter ++;
		return vfs_filesession_counter - 1;
	}

	ibis::file vfs_file_open(ibis::string _path)
	{
		kerror_push(__func__);
		ibis::file file_session(vfs_get_unique_filesession_id());

		vnode* file_node = vfs_get_path(_path);

		if (file_node == nullptr)
		{
			kerror_pop();
			return ibis::file(0);
		}

		vfilesession vfile_session = vfilesession(file_session, _path);

		vfs_filesessions->push(vfile_session);

		kerror_pop();
		return file_session;
	}

	ibis::status vfs_file_write(ibis::file _filesession, ibis::file_msg _msg)
	{
		kerror_push(__func__);
		for (memint i = 0; i < vfs_filesessions->length(); i ++)
		{
			if (_filesession.id() == (*vfs_filesessions)[i].id().id())
			{
				vnode* file = vfs_get_path((*vfs_filesessions)[i].node());

				if (file != nullptr)
				{
					file->write(_msg, &(*vfs_filesessions)[i]);
					kerror_pop();
					return ibis::status(ibis::result::SUCCESS);
				}
				else
				{
					kerror_pop();
					return ibis::status(ibis::result::FAILURE);
				}
			}
		}

		kerror_pop();
		return ibis::status(ibis::result::FAILURE);
	}

	ibis::status vfs_file_close(ibis::file _filesession)
	{
		for (memint i = 0; i < vfs_filesessions->length(); i ++)
		{
			if (_filesession.id() == (*vfs_filesessions)[i].id().id())
			{
				(*vfs_filesessions)[i] = vfs_filesessions->back();
				vfs_filesessions->pop();
				return ibis::status(ibis::result::SUCCESS);
			}
		}

		return ibis::status(ibis::result::FAILURE);
	}
};
