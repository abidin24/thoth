/*
* 	filename  : random.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/random.hpp>
#include <thoth/vfs/vfs.hpp>
#include <thoth/vfs/vnode.hpp>
#include <thoth/early.hpp>

namespace thoth
{
	ibis::status random_init(ibis::string _node)
	{
		thoth::vnode* dev_node = nullptr;
		thoth::vnode* random_node = nullptr;

		// Bind VGA driver to /dev/vga file hook
		dev_node = vfs_get_path("/dev");
		if (dev_node != nullptr)
			random_node = dev_node->add_vnode("random", VNODE_FLAG_TYPE_FILE);

		if (dev_node == nullptr)
			early_print("random driver : Failed to find vnode '/dev'\n");
		else if (random_node == nullptr)
			early_print("random driver : Failed to create vnode '/dev/random'\n");
		else
		{
			bool bind_success = random_node->bind_write_hook(random_process_msg);

			if (bind_success)
			{
				early_print("random driver : Successfully bound to '/dev/random' write hook\n");
			}
			else
				early_print("random driver : Failed to bind to '/dev/random' write hook\n");
		}

		return ibis::status(ibis::result::SUCCESS);
	}

	void random_process_msg(const ibis::file_msg _file_msg, const vfilesession* _session)
	{
		ibis::print_line("Random received data!");
	}
};
