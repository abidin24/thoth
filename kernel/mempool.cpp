/*
* 	filename  : mempool.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/mempool.hpp>
#include <thoth/early.hpp>
#include <thoth/kentry.hpp>
#include <thoth/kerror.hpp>

namespace thoth
{
	typedef byte map_entry_data;

	static const map_entry_data MAP_ENTRY_MASK_ALL  = ~(map_entry_data)0;
	static const map_entry_data MAP_ENTRY_MASK_NONE = (map_entry_data)0;

	static const map_entry_data MAP_ENTRY_ALL  = ~(map_entry_data)0;
	static const map_entry_data MAP_ENTRY_NONE = (map_entry_data)0;

	struct map_entry
	{
		enum class flag_type
		{
			CLEAR = 0,
			USED  = (1 << 0),
			HEAD  = (1 << 1),
		};

		map_entry_data _flags;

		void clear() { this->_flags = (map_entry_data)flag_type::CLEAR; }

		bool get_flag(flag_type _flag) { return (this->_flags & (map_entry_data)_flag) > 0; }
		void set_flag(flag_type _flag, bool _enabled) { if (_enabled) this->_flags |= (map_entry_data)_flag; else this->_flags &= ~(map_entry_data)_flag; }

		bool get_used() { return this->get_flag(flag_type::USED); }
		bool get_head() { return this->get_flag(flag_type::HEAD); }

		void set_used(bool _used) { this->set_flag(flag_type::USED, _used); }
		void set_head(bool _head) { this->set_flag(flag_type::HEAD, _head); }

		bool matches(map_entry_data _match, map_entry_data _mask = MAP_ENTRY_MASK_ALL) { return (this->_flags & _mask) == _match; }
		void flag(map_entry_data _flags, map_entry_data _mask = MAP_ENTRY_MASK_ALL) { this->_flags = (_flags & _mask) | (this->_flags & ~_mask); }
	};

	static const map_entry_data MAP_ENTRY_MASK_USED = (map_entry_data)map_entry::flag_type::USED;
	static const map_entry_data MAP_ENTRY_MASK_HEAD = (map_entry_data)map_entry::flag_type::HEAD;

	static const map_entry_data MAP_ENTRY_USED = (map_entry_data)map_entry::flag_type::USED;
	static const map_entry_data MAP_ENTRY_HEAD = (map_entry_data)map_entry::flag_type::HEAD;

	static const memint MAP_INDEX_INVALID = ~(memint)0;

	struct map_region
	{
		memint _start;
		memint _size;

		map_region(memint _start, memint _size) { this->_start = _start; this->_size = _size; }
		bool valid() { return this->_start != MAP_INDEX_INVALID && this->_size != MAP_INDEX_INVALID; }
	};

	static ptr    mempool_space_head = nullptr;
	static memint mempool_space_size = 0;

	static ptr    mempool_map_head = nullptr;
	static memint mempool_map_size = 0;

	static ptr    mempool_pool_head = nullptr;
	static memint mempool_pool_size = 0;

	static map_entry* mempool_map = nullptr;
	static memint     mempool_block_size = 0;
	static memint     mempool_block_count = 0;
	static bool       mempool_initiated = false;

	static ptr mempool_index_to_ptr(memint _index);
	static memint mempool_ptr_to_index(ptr _ptr);
	static memint mempool_size_to_mapcount(memint _size);

	static map_region mempool_find_region(memint _size, map_entry_data _match, map_entry_data _mask = MAP_ENTRY_MASK_ALL, memint _offset = 0);
	static bool       mempool_flag_region(map_region _region, map_entry_data _flags, map_entry_data _mask = MAP_ENTRY_MASK_ALL);

	static map_region mempool_find_chain(memint _index);

	static bool mempool_transcribe_block(memint _src, memint _dest);
	static bool mempool_transcribe_region(map_region _src, map_region _dest);

	static ibis::status     mempool_init_impl(ptr _ptr, memint _size, memint _blocksize);
	static ibis::value<ptr> mempool_alloc_impl(memint _n);
	static ibis::value<ptr> mempool_realloc_impl(ptr _ptr, memint _n);
	static ibis::status     mempool_dealloc_impl(ptr _ptr);

	static ptr mempool_index_to_ptr(memint _index)
	{
		if (_index > mempool_block_count) // Is the index out of bounds?
			return nullptr;

		return (ptr)((memint)mempool_pool_head + _index * mempool_block_size);
	}

	static memint mempool_ptr_to_index(ptr _ptr)
	{
		if (((memint)_ptr - (memint)mempool_pool_head) % mempool_block_size != 0) // Is the pointer not aligned correctly?
			return MAP_INDEX_INVALID;

		if (_ptr < mempool_pool_head || (memint)_ptr >= (memint)mempool_pool_head + mempool_pool_size) // Is the pointer out of bounds?
			return MAP_INDEX_INVALID;

		return ((memint)_ptr - (memint)mempool_pool_head) / mempool_block_size;
	}

	static memint mempool_size_to_mapcount(memint _size)
	{
		if (_size % mempool_block_size != 0) // If it's perfectly block-aligned
			return _size / mempool_block_size + 1;
		else // If it's not perfectly block-aligned
			return _size / mempool_block_size;
	}

	static map_region mempool_find_region(memint _size, map_entry_data _match, map_entry_data _mask, memint _offset)
	{
		memint cpos = _offset;
		memint csize = 0;

		for (memint i = _offset; i < mempool_block_count; i ++)
		{
			if (mempool_map[i].matches(_match, _mask)) // If we have a matching entry
				csize ++;
			else // If the entry does not match
			{
				cpos = i + 1;
				csize = 0;
			}

			if (csize >= _size) // If we've found a large enough region
				return map_region(cpos, csize);
		}

		return map_region(MAP_INDEX_INVALID, MAP_INDEX_INVALID);
	}

	static bool mempool_flag_region(map_region _region, map_entry_data _flags, map_entry_data _mask)
	{
		if (!_region.valid())
			return false;

		for (memint i = 0; i < _region._size; i ++)
		{
			if (_region._start + i >= mempool_block_count) // If we're outside the map bounds
				return false;
			else
				mempool_map[_region._start + i].flag(_flags, _mask); // Flag each entry
		}

		return true;
	}

	static map_region mempool_find_chain(memint _index)
	{
		memint cpos = _index;
		memint csize = 0;

		if (mempool_map[cpos].matches(MAP_ENTRY_USED | MAP_ENTRY_HEAD, MAP_ENTRY_MASK_USED | MAP_ENTRY_MASK_HEAD)) // If we've found a head entry
			csize ++;
		else // If we've haven't found a head entry
			return map_region(MAP_INDEX_INVALID, MAP_INDEX_INVALID);

		for (memint i = cpos + 1; i < mempool_block_count; i ++)
		{
			if (mempool_map[i].matches(MAP_ENTRY_USED, MAP_ENTRY_MASK_USED | MAP_ENTRY_MASK_HEAD)) // If we've found a tail entry
				csize ++;
			else // If we've not found a tail entry
				break;
		}

		return map_region(cpos, csize);
	}

	static bool mempool_transcribe_block(memint _src, memint _dest)
	{
		if (_src == _dest) // If we're transcribing the same block
			return false;

		ptr src_ptr = mempool_index_to_ptr(_src);
		ptr dest_ptr = mempool_index_to_ptr(_dest);

		if (src_ptr == nullptr || dest_ptr == nullptr) // If either map index was invalid
			return false;

		for (memint i = 0; i < mempool_block_size; i ++)
		{
			((byte*)dest_ptr)[i] = ((byte*)src_ptr)[i];
		}

		return true;
	}

	static bool mempool_transcribe_region(map_region _src, map_region _dest)
	{
		if (!_src.valid() || !_dest.valid()) // If either src or dest are invalid
			return false;

		if ((_src._start <= _dest._start && _dest._start < _src._start + _src._size) || (_dest._start <= _src._start && _src._start < _dest._start + _dest._size)) // If an overlap occurs
			return false;

		for (memint i = 0; (i < _src._size) && (i < _dest._size); i ++)
		{
			if (!mempool_transcribe_block(_src._start + i, _dest._start + i)) // If the transcribe failed
				return false;
		}

		return true;
	}

	static ibis::status mempool_init_impl(ptr _ptr, memint _size, memint _blocksize)
	{
		if (mempool_initiated)
			return ibis::status(ibis::result::FAILURE);

		mempool_space_head = _ptr;
		mempool_space_size = _size;

		mempool_block_size = _blocksize;
		mempool_block_count = mempool_space_size / (sizeof(map_entry) + mempool_block_size);

		mempool_map_head = mempool_space_head;
		mempool_map_size = mempool_block_count * sizeof(map_entry);

		mempool_pool_head = (ptr)((memint)mempool_map_head + mempool_map_size);
		mempool_pool_size = mempool_block_count * mempool_block_size;

		mempool_map = (map_entry*)mempool_map_head;

		mempool_initiated = true;

		return ibis::status(ibis::result::SUCCESS);
	}

	static ibis::value<ptr> mempool_alloc_impl(memint _n)
	{
		memint block_num = mempool_size_to_mapcount(_n); // How many blocks will we need?

		map_region new_region = mempool_find_region(block_num, 0, MAP_ENTRY_MASK_USED); // Find an appropriate region with no USED flags

		if (new_region.valid())
		{
			mempool_flag_region(map_region(new_region._start, 1), MAP_ENTRY_USED | MAP_ENTRY_HEAD, MAP_ENTRY_MASK_USED | MAP_ENTRY_MASK_HEAD); // Flag the new head

			if (new_region._size > 1)
				mempool_flag_region(map_region(new_region._start + 1, new_region._size - 1), MAP_ENTRY_USED, MAP_ENTRY_MASK_USED | MAP_ENTRY_MASK_HEAD); // Flag the new tails

			return ibis::value<ptr>(mempool_index_to_ptr(new_region._start), ibis::result::SUCCESS);
		}
		else
			return ibis::value<ptr>(nullptr, ibis::result::FAILURE);
	}

	static ibis::value<ptr> mempool_realloc_impl(ptr _ptr, memint _n)
	{
		ibis::value<ptr> dest_ptr = mempool_alloc(_n);

		if (!dest_ptr.success())
			return ibis::value<ptr>(nullptr, ibis::result::FAILURE);

		memint src_index = mempool_ptr_to_index(_ptr);
		memint dest_index = mempool_ptr_to_index(dest_ptr.data());

		if (src_index == MAP_INDEX_INVALID || dest_index == MAP_INDEX_INVALID)
			return ibis::value<ptr>(nullptr, ibis::result::FAILURE);

		memint block_num = mempool_size_to_mapcount(_n);

		if (!mempool_transcribe_region(map_region(src_index, block_num), map_region(dest_index, block_num)))
			return ibis::value<ptr>(nullptr, ibis::result::FAILURE);

		if (!mempool_dealloc(_ptr).success())
			return ibis::value<ptr>(nullptr, ibis::result::FAILURE);

		return ibis::value<ptr>(dest_ptr.data(), ibis::result::SUCCESS);
	}

	static ibis::status mempool_dealloc_impl(ptr _ptr)
	{
		memint map_index = mempool_ptr_to_index(_ptr);

		if (map_index == MAP_INDEX_INVALID)
			return ibis::status(ibis::result::FAILURE);

		map_region mem_region = mempool_find_chain(map_index);

		if (mem_region.valid())
		{
			mempool_flag_region(mem_region, MAP_ENTRY_NONE, MAP_ENTRY_ALL); // Flag everything to nothing
			return ibis::status(ibis::result::SUCCESS);
		}
		else
			return ibis::status(ibis::result::FAILURE);
	}

	ibis::status mempool_init(ptr _ptr, memint _size, memint _blocksize)
	{
		kerror_push(__func__);

		ibis::status result = mempool_init_impl(_ptr, _size, _blocksize);

		if (!result.success())
			kerror("Failure in mempool initiation");

		kerror_pop();
		return result;
	}

	ibis::value<ptr> mempool_alloc(memint _n)
	{
		kerror_push(__func__);

		ibis::value<ptr> result = mempool_alloc_impl(_n);

		if (!result.success())
			kerror("Failure in mempool allocation");

		kerror_pop();
		return result;
	}

	ibis::value<ptr> mempool_realloc(ptr _ptr, memint _n)
	{
		kerror_push(__func__);

		ibis::value<ptr> result = mempool_realloc_impl(_ptr, _n);

		if (!result.success())
			kerror("Failure in mempool reallocation");

		kerror_pop();
		return result;
	}

	ibis::status mempool_dealloc(ptr _ptr)
	{
		kerror_push(__func__);

		ibis::status result = mempool_dealloc_impl(_ptr);

		if (!result.success())
			kerror("Failure in mempool deallocation");

		kerror_pop();
		return result;
	}

	ibis::value<int> mempool_get_usage()
	{
		return ibis::value<int>(0, ibis::result::FAILURE);
	}

	ibis::value<int> mempool_get_fragmentation()
	{
		return ibis::value<int>(0, ibis::result::FAILURE);
	}

	ibis::status mempool_display(memint _n)
	{
		if (!mempool_initiated)
			return ibis::status(ibis::result::FAILURE);

		early_print("-- Memory Pool --\n");

		for (memint i = 0; i < _n && i < mempool_block_count; i ++)
		{
			if (mempool_map[i].matches(MAP_ENTRY_USED, MAP_ENTRY_MASK_USED))
			{
				if (mempool_map[i].matches(MAP_ENTRY_HEAD, MAP_ENTRY_MASK_HEAD))
					early_print("H");
				else
					early_print("T");
			}
			else
				early_print("-");
		}

		early_print("\n");

		return ibis::status(ibis::result::SUCCESS);
	}

};
