/*
* 	filename  : ibis.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/ibis.hpp>
#include <thoth/early.hpp>

// Ibis headers
#include <ibis/stdio.hpp>

namespace ibis
{
	extern file stdout;
}

namespace thoth
{
	static bool ibis_initiated_state = false;

	void ibis_init(ibis::string _in, ibis::string _out)
	{
		ibis::stdout = ibis::open(_out);

		if (!ibis::stdout.valid())
			early_print((ibis::string("Failed to open Ibis stdout as '") + _out + "'\n").data());

		ibis_initiated_state = true;
	}

	bool ibis_initiated()
	{
		return ibis_initiated_state;
	}
};
