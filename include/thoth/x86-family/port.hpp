/*
* 	filename  : port.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_x86_FAMILY_PORT_HPP
#define _THOTH_x86_FAMILY_PORT_HPP

// Ibis headers
#include <ibis/value.hpp>

namespace thoth
{
	ibis::status port_out8(uint16 _port, uint8 _value);
	ibis::status port_out16(uint16 _port, uint16 _value);
	ibis::status port_out32(uint16 _port, uint32 _value);

	ibis::value<uint8>  port_in8(uint16 _port);
	ibis::value<uint16> port_in16(uint16 _port);
	ibis::value<uint32> port_in32(uint16 _port);
};

// End header guard
#endif
