/*
* 	filename  : earlyvga.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_x86_FAMILY_EARLYVGA_HPP
#define _THOTH_x86_FAMILY_EARLYVGA_HPP

// Ibis headers
#include <ibis/value.hpp>

// Local headers
#include "vgadef.hpp"

namespace thoth
{
	extern vga_colour early_vga_fg;
	extern vga_colour early_vga_bg;
	extern memint     early_vga_cursor_column;
	extern memint     early_vga_cursor_row;

	void early_clear_vga();
	void early_print_vga(const char* _str);
};

// End header guard
#endif
