/*
* 	filename  : mempool.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_MEMPOOL_HPP
#define _THOTH_MEMPOOL_HPP

// Ibis headers
#include <ibis/value.hpp>

namespace thoth
{
	/*
	ibis::status       mempool_init(ptr _ptr, memint _size, memint _blocksize);
	ibis::value<ptr>   mempool_alloc(memint _n);
	ibis::value<ptr>   mempool_realloc(ptr _ptr, memint _n);
	ibis::status       mempool_dealloc(ptr _ptr);
	ibis::value<int>   mempool_get_usage();
	ibis::value<int>   mempool_get_fragmentation();
	ibis::status       mempool_display(memint _n);
	*/

	ibis::status       mempool_init(ptr _ptr, memint _size, memint _blocksize);
	ibis::value<ptr>   mempool_alloc(memint _n);
	ibis::value<ptr>   mempool_realloc(ptr _ptr, memint _n);
	ibis::status       mempool_dealloc(ptr _ptr);
	ibis::value<int>   mempool_get_usage();
	ibis::value<int>   mempool_get_fragmentation();
	ibis::status       mempool_display(memint _n);
};

// End header guard
#endif
