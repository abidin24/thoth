/*
* 	filename  : vnode.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_VFS_VNODE_HPP
#define _THOTH_VFS_VNODE_HPP

// Ibis headers
#include <ibis/generic/vector.hpp>
#include <ibis/string.hpp>
#include <ibis/stdio.hpp>

// Thoth headers
#include <thoth/vfs/vfilesession.hpp>

namespace thoth
{
	// Forward declaration
	struct vfilesession;

	typedef byte vnode_flag_data;

	static const vnode_flag_data VNODE_FLAG_MASK_ALL  = ~(vnode_flag_data)0;
	static const vnode_flag_data VNODE_FLAG_MASK_NONE = (vnode_flag_data)0;

	static const vnode_flag_data VNODE_FLAG_ALL  = ~(vnode_flag_data)0;
	static const vnode_flag_data VNODE_FLAG_NONE = (vnode_flag_data)0;

	enum class vnode_flag_type
	{
		CLEAR = 0,
		DIR   = (1 << 0),
		ROOT  = (1 << 1),
	};

	struct vnode_flags
	{
		vnode_flag_data _flags;

		void clear() { this->_flags = (vnode_flag_data)vnode_flag_type::CLEAR; }

		bool get_flag(vnode_flag_type _flag) { return (this->_flags & (vnode_flag_data)_flag) > 0; }
		void set_flag(vnode_flag_type _flag, bool _enabled) { if (_enabled) this->_flags |= (vnode_flag_data)_flag; else this->_flags &= ~(vnode_flag_data)_flag; }

		bool get_dir() { return this->get_flag(vnode_flag_type::DIR); }
		bool get_root() { return this->get_flag(vnode_flag_type::ROOT); }

		void set_dir(bool _dir) { this->set_flag(vnode_flag_type::DIR, _dir); }
		void set_root(bool _root) { this->set_flag(vnode_flag_type::ROOT, _root); }

		bool matches(vnode_flag_data _match, vnode_flag_data _mask = VNODE_FLAG_MASK_ALL) { return (this->_flags & _mask) == _match; }
		void flag(vnode_flag_data _flags, vnode_flag_data _mask = VNODE_FLAG_MASK_ALL) { this->_flags = (_flags & _mask) | (this->_flags & ~_mask); }
	};

	static const vnode_flag_data VNODE_FLAG_MASK_DIR = (vnode_flag_data)vnode_flag_type::DIR;
	static const vnode_flag_data VNODE_FLAG_MASK_ROOT = (vnode_flag_data)vnode_flag_type::ROOT;

	static const vnode_flag_data VNODE_FLAG_DIR = (vnode_flag_data)vnode_flag_type::DIR;
	static const vnode_flag_data VNODE_FLAG_ROOT = (vnode_flag_data)vnode_flag_type::ROOT;

	static const vnode_flag_data VNODE_FLAG_TYPE_FILE = (vnode_flag_data)0;
	static const vnode_flag_data VNODE_FLAG_TYPE_DIR = (vnode_flag_data)vnode_flag_type::DIR;
	static const vnode_flag_data VNODE_FLAG_TYPE_ROOTDIR = (vnode_flag_data)vnode_flag_type::ROOT | (vnode_flag_data)vnode_flag_type::DIR;

	struct vnode
	{
		ibis::string _name;
		vnode_flags _flags;

		ibis::vector<vnode*> _children;

		void (*func_write)(const ibis::file_msg, const vfilesession* _msg) = nullptr;

		vnode(ibis::string _name = "", vnode_flag_data _flags = VNODE_FLAG_TYPE_FILE);

		void init(ibis::string _name = "", vnode_flag_data _flags = VNODE_FLAG_TYPE_FILE);

		vnode* get_vnode(ibis::string _name);
		vnode* get_path(ibis::string _name);

		vnode* add_vnode(ibis::string _name, vnode_flag_data _flags = VNODE_FLAG_TYPE_FILE);

		ibis::string          name();
		vnode_flags&          flags() { return this->_flags; }
		ibis::vector<vnode*>& children();

		void write(const ibis::file_msg _msg, const vfilesession* _session);
		bool bind_write_hook(void (*func_write)(const ibis::file_msg, const vfilesession*));
	};
};

// End header guard
#endif
