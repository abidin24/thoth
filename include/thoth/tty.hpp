/*
* 	filename  : tty.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_TTY_HPP
#define _THOTH_TTY_HPP

// Ibis headers
#include <ibis/value.hpp>
#include <ibis/string.hpp>
#include <ibis/stdio.hpp>

namespace thoth
{
	struct tty
	{
		unsigned long _id = 0;
		ibis::string _path;

		tty(unsigned long _id, ibis::string _path);

		ibis::status init();

		unsigned long id() { return this->_id; }
		ibis::string  path() { return this->_path; }

		void process_msg(const ibis::file_msg _file_msg);
	};
};

// End header guard
#endif
