/*
* 	filename  : ringbuffer.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_GENERIC_RINGBUFFER_HPP
#define _IBIS_GENERIC_RINGBUFFER_HPP

// Ibis headers
#include <ibis/memory.hpp>

namespace ibis
{
	template <typename T>
	struct vector
	{
	private:

			T* _data = nullptr;
			memint _size = 0;

	public:

		vector(memint _size)
		{
			this->_size = _size;
		};

		~vector()
		{
			if (this->_data != nullptr)
				delete this->_data;
		}

		status push(T _item)
		{
			this->preupdate();

			this->_data[this->_len] = _item;
			this->_len ++;

			this->postupdate();

			return status(result::SUCCESS);
		}

		T pop()
		{
			this->preupdate();

			T _item = this->_data[this->_len - 1];
			this->_len --;

			this->postupdate();

			return _item;
		}

		T back()
		{
			return this->_data[this->_len - 1];
		}

		T& operator[](memint i)
		{
			return this->_data[i];
		}

		memint length()
		{
			return this->_len;
		}

	private:

		void preupdate()
		{
			if (this->_len >= this->_size)
			{
				this->_size *= 2;
				this->_data = (T*)reallocate(this->_data, sizeof(T) * this->_size).data();
			}
		}

		void postupdate()
		{
			if (this->_size >= 2 && this->_size >= this->_len * 3)
			{
				this->_size /= 2;
				this->_data = (T*)reallocate(this->_data, sizeof(T) * this->_size).data();
			}
		}
	};
}

// End header guard
#endif
