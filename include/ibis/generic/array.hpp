/*
* 	filename  : array.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_GENERIC_ARRAY_HPP
#define _IBIS_GENERIC_ARRAY_HPP

// Ibis headers
#include <ibis/types.hpp>

namespace ibis
{
	template <typename T>
	struct array
	{
	private:

		T* _data;
		memint _len;

	public:

		Array(memint _n);
		array<T>  operator=(const array<T>& _other);
		array<T>  operator+(const array<T>& _other);
		array<T>& operator+=(const array<T>& _other);
		T&        operator[](memint i) const;
		array<T>  operator=(const T* _str);
		bool      operator==(const array<T>& _other) const;
	};
}

// End header guard
#endif
