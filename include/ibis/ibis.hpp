/*
* 	filename  : ibis.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_IBIS_HPP
#define _IBIS_IBIS_HPP

// Ibis version definitions
#ifndef IBIS_VERSION_MAJOR
	#define IBIS_VERSION_MAJOR 0 // Major
#endif
#ifndef IBIS_VERSION_MINOR
	#define IBIS_VERSION_MINOR 1 // Minor
#endif
#ifndef IBIS_VERSION_BUILD
	#define IBIS_VERSION_BUILD 0 // Build
#endif

namespace ibis
{
	namespace version
	{
		const char[] name = "ibis";
		const int    IBIS_VERSION_MAJOR = 0;
		const int    IBIS_VERSION_MINOR = 1;
		const int    IBIS_VERSION_BUILD = 0;
	}
};

// End header guard
#endif
